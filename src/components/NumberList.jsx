import { Component } from "react";

export default class NumberList extends Component {
  render() {
    return this.props.list.map((item) => <p>{item}</p>);
  }
}

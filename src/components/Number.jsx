import { Component } from "react";

export default class Number extends Component {
  render() {
    return (
      <div
        style={{
          border: "solid green 1px",
        }}
      >
        {this.props.children}
      </div>
    );
  }
}

import { Component } from "react";
import "./App.css";
import Number from "./components/Number";
import NumberList from "./components/NumberList";

class App extends Component {
  state = {
    numberList: [],
  };

  addNumber = () => {
    let list = this.state.numberList;
    return list.length === 0
      ? this.setState({
          numberList: [10],
        })
      : this.setState({
          numberList: [...list, list[list.length - 1] + 10],
        });
  };

  render() {
    return (
      <>
        <button onClick={this.addNumber}>CLICK</button>
        <Number number>
          <NumberList list={this.state.numberList} />
        </Number>
      </>
    );
  }
}

export default App;
